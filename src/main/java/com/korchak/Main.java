package com.korchak;

import java.util.Scanner;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;;
import org.hibernate.SessionFactory;

import org.hibernate.query.Query;

public class Main {

  private static SessionFactory sessionFactory;

  static {
    try {
      sessionFactory = new Configuration().configure().buildSessionFactory();
    } catch (Exception e) {
      System.out.println("exception");
      e.printStackTrace();
    }
  }

  public static Session getSession() {
    return sessionFactory.openSession();
  }


  public static void main(String[] args) throws Exception {

//    Session session = getSession();

    try (Session session = getSession()) {
//      readAllTables(session);
      readStudentsOfGroup(session);
//      findStudentByName(session);
//      addStudentToGroup(session);

    } catch (Exception e) {

      e.printStackTrace();

    }

  }

  public static void readAllTables(Session session) {
    System.out.println("from student table---------");
    Query query = session.createQuery("from " + "StudentEntity");
    System.out.format("%-5s %-12s %-12s %s \n", "id", "name", "surname", "email");
    for (Object object : query.list()) {
      StudentEntity student = (StudentEntity) object;
      System.out.format("%-5s %-12s %-12s %s \n", student.getStudentId(), student.getName(),
          student.getSurname(), student.getEmail());
    }

    System.out.println("from lab_group table---------");
    query = session.createQuery("from " + "LabGroupEntity ");
    System.out.format("%-5s %-12s \n", "id", "group");
    for (Object object : query.list()) {
      LabGroupEntity group = (LabGroupEntity) object;
      System.out.format("%-5s %-12s  \n", group.getGroupId(), group.getGroupName());
    }

  }

  public static void readStudentsOfGroup(Session session) {
    String groupIn = "";
//    Scanner in = new Scanner(System.in);

    try (Scanner in = new Scanner(System.in);) {
      System.out.println("enter name of group:");
      groupIn = in.nextLine();

    } catch (Exception e) {
      e.printStackTrace();
    }

    Query query = session.createQuery("from LabGroupEntity where groupName =:group1");
    query.setParameter("group1", groupIn);
    LabGroupEntity groupEntity = (LabGroupEntity) query.list().get(0);
    if (groupEntity != null) {
      if (groupEntity.getStudents() != null) {
        System.out.format("%-5s %-12s %-12s %s \n", "id", "name", "surname", "email");
        for (StudentEntity student : groupEntity.getStudents()) {
          System.out.format("%-5s %-12s %-12s %s \n", student.getStudentId(), student.getName(),
              student.getSurname(), student.getEmail());
        }
      } else {
        System.out.println("there is no student in this group");
      }

    } else {
      System.out.println("there is no group like this");
    }
  }

  public static void findStudentByName(Session session) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("enter student name:");
    String studentIn = scanner.nextLine();

    Query query = session.createQuery("from StudentEntity where name =:name1");
    query.setParameter("name1", studentIn);
    StudentEntity student = null;
    if (!query.list().isEmpty()) {
      student = (StudentEntity) query.list().get(0);
    }
    if (student != null) {
      System.out.format("%-5s %-12s %-12s %s \n", "id", "name", "surname", "email");
      System.out.format("%-5s %-12s %-12s %s \n", student.getStudentId(), student.getName(),
          student.getSurname(), student.getEmail());
      if (student.getGroups() != null) {
        for (LabGroupEntity group : student.getGroups()) {
          System.out.format("%-5s %s \n", "id", "group");
          System.out.format("%-5s %s\n", group.getGroupId(), group.getGroupName());
        }
      } else {
        System.out.println("there is no group related to this student");
      }
    } else {
      System.out.println("there is no student like this");
    }
  }

  public static void addStudentToGroup(Session session) {
    Scanner scanner = new Scanner(System.in);
    String groupIn;
    System.out.println("enter name of student:");
    String studentIn = scanner.nextLine();

    Query query = session.createQuery("from StudentEntity where name =:name1");
    query.setParameter("name1", studentIn);
    StudentEntity student = null;
    if (!query.list().isEmpty()) {
      student = (StudentEntity) query.list().get(0);
    }
    if (student != null) {
      System.out.println("enter to which group add student " + studentIn);
      groupIn = scanner.nextLine();

      query = session.createQuery("from LabGroupEntity where groupName =:group1");
      query.setParameter("group1", groupIn);
      LabGroupEntity group = null;
      if (!query.list().isEmpty()) {
        group = (LabGroupEntity) query.list().get(0);
      }
      if (group != null) {
        session.beginTransaction();
        group.addStudent(student);
        session.save(group);
        session.getTransaction().commit();
        System.out.println("student added to group " + groupIn);
      } else {
        System.out.println("there is no group with this name");
      }
    } else {
      System.out.println("there is no student like this in base");
    }


  }
}
