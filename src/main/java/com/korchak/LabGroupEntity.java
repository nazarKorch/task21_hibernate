package com.korchak;

import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "lab_group", schema = "people")
public class LabGroupEntity {

  @Id
  @Column(name = "group_id", nullable = false)
  private int groupId;

  @Basic
  @Column(name = "group_name", nullable = false, length = 30)
  private String groupName;

  @ManyToMany(mappedBy = "groups")
  private List<StudentEntity> students;

  public LabGroupEntity(){}

  public List<StudentEntity> getStudents() {
    return students;
  }

  public void setStudents(List<StudentEntity> students){
    this.students = students;
  }

  public void addStudent(StudentEntity student){
    if(!getStudents().contains(student)){
      getStudents().add(student);
    }
    if(!student.getGroups().contains(this)){
      student.getGroups().add(this);
    }
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }


  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    LabGroupEntity that = (LabGroupEntity) o;

    if (groupId != that.groupId) {
      return false;
    }
    if (groupName != null ? !groupName.equals(that.groupName) : that.groupName != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = groupId;
    result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
    return result;
  }
}
