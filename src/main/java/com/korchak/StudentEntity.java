package com.korchak;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "student", schema = "people")
public class StudentEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "student_id")
  private int studentId;

  @Basic
  @Column(name = "name", nullable = false, length = 30)
  private String name;

  @Basic
  @Column(name = "surname", nullable = false, length = 40)
  private String surname;

  @Basic
  @Column(name = "email", nullable = true, length = 40)
  private String email;

  @ManyToMany
  @JoinTable(name = "student_has_group", schema = "people",
      joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "student_id"),
      inverseJoinColumns = @JoinColumn(name = "group_id", referencedColumnName = "group_id"))
  private List<LabGroupEntity> groups;

  public StudentEntity(){}

  public List<LabGroupEntity> getGroups() {
    return groups;
  }

  public void setGroups(List<LabGroupEntity> groups){
    this.groups = groups;
  }

  public void addGroup(LabGroupEntity group){
    if(!getGroups().contains(group)){
      getGroups().add(group);
    }
    if(!group.getStudents().contains(this)){
      group.getStudents().add(this);
    }
  }

  public int getStudentId() {
    return studentId;
  }

  public void setStudentId(int studentId) {
    this.studentId = studentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    StudentEntity that = (StudentEntity) o;

    if (studentId != that.studentId) {
      return false;
    }
    if (name != null ? !name.equals(that.name) : that.name != null) {
      return false;
    }
    if (surname != null ? !surname.equals(that.surname) : that.surname != null) {
      return false;
    }
    if (email != null ? !email.equals(that.email) : that.email != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = studentId;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (surname != null ? surname.hashCode() : 0);
    result = 31 * result + (email != null ? email.hashCode() : 0);
    return result;
  }
}
